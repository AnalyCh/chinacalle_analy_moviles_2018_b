package analy.moviles.epn.chinacalle_analy_moviles_2018_b

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.PersistableBundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class ActivityJuego : AppCompatActivity() {

    internal lateinit var puntajeTextView: TextView
    internal lateinit var btnRandom: Button
    internal lateinit var btnAceptar: Button
    internal var score = 0
    internal var random = 0

    internal lateinit var countDownTimer: CountDownTimer
    internal var countDownInterval = 1000L
    internal var initialCountDown = 10000L
    internal var timeLeft = 10

    internal var gameStarted = false

    internal var TAG = ActivityJuego::class.java.simpleName
    companion object {              //Similar a una clase dentro de una clase
        private val SCORE_KEY = "SCORE_KEY"
        private val TIME_LEFT_KEY = "TIME_LEFT_KEY"
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_juego)

        //estado Log.d(TAG,  "onCreate called. Score is $score")
        Log.d(TAG,  "onCreate called. Score is $score")


        puntajeTextView = findViewById(R.id.puntajeTextView)
        btnRandom = findViewById(R.id.btnRandom)
        btnAceptar = findViewById(R.id.btnAceptar)

        if(savedInstanceState != null){
            score = savedInstanceState.getInt(SCORE_KEY)
            timeLeft = savedInstanceState.getInt(TIME_LEFT_KEY)
            //restoreGame()
        }else {
            resetGame()
        }


        btnRandom.setOnClickListener{_ -> esperarAceptar()}


    }

    private fun esperarAceptar(){
        btnAceptar.setOnClickListener{_ -> incrementScore()}
        if(!gameStarted){
            startGame()
        }
    }

    private fun incrementScore(){


        if(momentoJusto()){
           score+=100
        }
        else if(segundoDiferente()){
            score+=50
        }
        val newScore = getString(R.string.puntaje, Integer.toString(score))
        puntajeTextView.text = newScore

    }

    private  fun momentoJusto():Boolean{
        var resultado = false
        val n = generarRandom()
        if(n == timeLeft){
            resultado = true
        }

        return resultado

    }
    private fun segundoDiferente():Boolean{
        var resultado = false

        val n = generarRandom()
        if(n == (timeLeft-1)|| n == (timeLeft+1) ){
            resultado = true
        }

        return resultado
    }
    private fun generarRandom(): Int{
        random = (0..10).shuffled().first()

        return random
    }



    private fun startGame(){
        countDownTimer.start()
    }



    private fun resetGame(){
        score = 0
        timeLeft = 10
        val gameScore = getString(R.string.puntaje, Integer.toString(score))
        puntajeTextView.text = gameScore


        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() /1000

            }

            override fun onFinish() {
                endGame()
            }
        }
        gameStarted = false

    }

    private fun endGame() {
        Toast.makeText(this,R.string.game_over_message, Toast.LENGTH_LONG).show()
        mostrarPuntaje()
        resetGame()

    }

    private fun mostrarPuntaje(){

    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)

        outState!!.putInt(SCORE_KEY,score)
        outState!!.putInt(TIME_LEFT_KEY, timeLeft)
        countDownTimer.cancel()

        Log.d(TAG, "onSaveInstanceState: score = $score n timeleft = $timeLeft")
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.d(TAG, "onDestroy called")
    }

    class Puntaje(val ronda: Int, val puntaje: Int){
        fun print(){

        }
    }


}
